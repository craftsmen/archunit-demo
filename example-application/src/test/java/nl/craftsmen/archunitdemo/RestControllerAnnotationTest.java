package nl.craftsmen.archunitdemo;

import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

@AnalyzeClasses(packages = "nl.craftsmen.archunitdemo")
public class RestControllerAnnotationTest {

    @ArchTest
    public static final ArchRule restControllerNamingRule = classes()
            .that()
            .resideInAPackage("..api..")
            .and()
            .haveSimpleNameEndingWith("Controller")
            .should()
            .beAnnotatedWith("org.springframework.web.bind.annotation.RestController");

}
