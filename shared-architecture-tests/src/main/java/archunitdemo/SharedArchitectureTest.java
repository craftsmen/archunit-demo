package archunitdemo;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.library.Architectures;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static com.tngtech.archunit.library.Architectures.onionArchitecture;

@AnalyzeClasses(importOptions = { ImportOption.DoNotIncludeArchives.class, ImportOption.DoNotIncludeJars.class})
public class SharedArchitectureTest {

    @ArchTest
    public static final ArchRule layeredArchitectureRule = Architectures.layeredArchitecture()
            .layer("Api").definedBy("..api..")
            .layer("Core").definedBy("..core..")
            .layer("Client").definedBy("..client..")

            .whereLayer("Api").mayNotBeAccessedByAnyLayer()
            .whereLayer("Core").mayOnlyBeAccessedByLayers("Api", "Client")
            .whereLayer("Client").mayNotBeAccessedByAnyLayer();


    @ArchTest
    public static final ArchRule onionModelRule = classes()
            .that()
            .resideInAPackage("..core..")
            .should()
            .onlyHaveDependentClassesThat()
            .resideInAnyPackage("..core..", "..api..", "..client..");

    @ArchTest
    public static final ArchRule onionRule = onionArchitecture()
            .domainModels("..core..")
            .domainServices("..core..")
            .applicationServices("..core..")
            .adapter("client", "..client..")
            .adapter("api", "..api..");

}
